package config

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

var db *sql.DB
var errInit error

func DatabaseInit() error {
	connStr := fmt.Sprintf(
		"%s://%s:%s@%s:%s/%s?sslmode=disable",
		GetEnv("DB_TYPE"),
		GetEnv("DB_USER"),
		GetEnv("DB_PASS"),
		GetEnv("DB_HOST"),
		GetEnv("DB_PORT"),
		GetEnv("DB_NAME"),
	)

	db, errInit = sql.Open(GetEnv("DB_TYPE"), connStr)
	if errInit != nil {
		return errInit
	}

	return nil
}

func GetDB() *sql.DB {
	return db
}
