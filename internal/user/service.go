package user

import (
	"database/sql"
	"errors"

	"golang.org/x/crypto/bcrypt"
)

func checkValidUser(userInput UserInput) (int, error) {
	var user User
	user, errQuery := queryUser(userInput)
	if errQuery != nil {
		if errQuery == sql.ErrNoRows {
			return -1, errors.New("unauthorized")
		}

		return -1, errors.New("internal error")
	}

	if ok := checkHashPassword(userInput.Password, user.Password); ok {
		return user.ID, nil
	}

	return -1, errors.New("unauthorized")

}
func checkHashPassword(passwordInput, hash string) bool {
	errHash := bcrypt.CompareHashAndPassword([]byte(hash), []byte(passwordInput))

	return errHash == nil
}
