package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"local.com/roadmap-cookie/config"
	"local.com/roadmap-cookie/internal/secret"
	"local.com/roadmap-cookie/internal/user"
	"local.com/roadmap-cookie/pkg/middleware/authentication"
)

func main() {
	errLoadEnv := config.LoadEnv()
	if errLoadEnv != nil {
		fmt.Println(errLoadEnv)
	}

	errDatabaseInit := config.DatabaseInit()
	if errDatabaseInit != nil {
		fmt.Println(errDatabaseInit)
	}

	errMigrate := config.RunMigration()
	if errMigrate != nil {
		fmt.Println(errMigrate)
	}
	config.SessionInit()

	r := mux.NewRouter()

	userRoute := r.PathPrefix("/user").Subrouter()
	// userRoute.Use(authentication.AuthMiddlware)
	userRoute.HandleFunc("/login", user.LoginFunc).Methods("POST")

	secretRoute := r.PathPrefix("/secret").Subrouter()
	secretRoute.Use(authentication.AuthMiddlware)
	secretRoute.HandleFunc("/", secret.SecretFunc).Methods("POST")

	errStart := http.ListenAndServe(":8081", r)
	if errStart != nil {
		fmt.Println("error starting server at port 8080")
	}

}
